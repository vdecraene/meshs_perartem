# Projet PERATEM émergent MESHS 2023-2024, script d'enrichissement des métadonnées

## Objectifs:
- A partir d'un export des tables journalistes et auteurs de la BDD Heurist, interroger l'API SRU de la BnF, interroger l'API IDRef
- Extraire les métadonnées adaptées afin d'enrichir les tables d'origines

## Responsables:
- Porteuse du projet: Carola Haenel Mesnard, U-Lille
- Saisie des données: Gabrielle Desmet (U-Lille)
- Développement du script: Valentin de Craene, MESHS-CNRS

# Workflow:
- Extraction des chaînes de caractères à rechercher via l'API au sein des CSV initiaux, dans /input (lib: Pandas)
- Génération des requêtes sur l'API SRU en "aut.accesspoint all " pour les récupérer le maximum de métadonnées possibles (lib: requests et urllib)
- Analyse de la réponse de l'API en XML et extraction des informations pertinentes sous forme de chaînes de caractères (lib: BS4 et Pandas)
- A partir du PPN_Z, interroger l'API IDRef pour récupérer l'ID VIAF, les informations cataloguées, l'ID AFNOR, l'URL Wikipedia
- Génération de nouvelles colonnes dans les CSV enrichis dans /output (lib:Pandas)
- Réinsertion des métadonnées enrichies dans les colonnes supplémentaires (lib:Pandas)

# Utilisation

`sudo apt-get install python3 libfreetype6-dev python3-pip python3-virtualenv`

`git clone https://gitlab.com/kopino4-templates/readme-template`

`virtualenv ~/.perartem_datalinking -p python3`

`source ~/.perartem_datalinking/bin/activate`

`pip install -r requirements.txt`


