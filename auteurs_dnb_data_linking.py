import csv
import xml.etree.ElementTree as ET
import urllib.parse
import requests
import pandas as pd
from bs4 import BeautifulSoup
import time



def get_dnb_info(name):
    encoded_query = urllib.parse.quote(f"WOE={name} and BBG=Tp*")
    base_url = f"https://services.dnb.de/sru/authorities?version=1.1&operation=searchRetrieve&query={encoded_query}&recordSchema=MARC21-xml"
    print(base_url)
    try:
        response = requests.get(base_url)
        response.raise_for_status()
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'xml')
            dnb_id = None
            dates_dnb = None
            page_wikipedia_de = None
            infos_supp = None



            datafield_548 = soup.find('datafield', tag="548")
            datafield_024 = soup.find('datafield', tag="024")
            datafield_400 = soup.find('datafield', tag="400")
            datafield_678 = soup.find('datafield', tag="678")
            datafield_670 = soup.find('datafield', tag="670")



            # --------


            # bnd_id
            if datafield_024:
                subfield_code_0 = datafield_024.find('subfield', code="0")
                if subfield_code_0:
                    dnb_id = subfield_code_0.text

            #dates_DNB
            if datafield_548:
                subfield_code_a = datafield_548.find('subfield', code="a")
                if subfield_code_a:
                    dates_dnb = subfield_code_a.text
            #page_wikipedia
            subfields = soup.find_all('subfield')
            for subfield in subfields:
                if subfield.text.startswith("http://de") or subfield.text.startswith("https://de") :
                    page_wikipedia_de = subfield.text
                    print(page_wikipedia_de)


            if datafield_678:
                subfield_code_b = datafield_678.find('subfield', code="b")
                if subfield_code_b:
                    infos_supp = subfield_code_b.text



            return dnb_id, dates_dnb, page_wikipedia_de, infos_supp
        else:
            return None, None, None, None
    except requests.RequestException as e:
        print(f"Erreur lors de la requête pour {name}: {e}")
        return None, None, None, None

input_csv = 'input/auteurs_perartem.csv'
output_csv = 'output/auteurs_data_linked_dnb.csv'

df = pd.read_csv(input_csv, sep='\t')

# Vérification de l'existence de la colonne
if 'Nom_Auteur' not in df.columns:
    print("La colonne 'Nom_Auteur' n'existe pas dans le fichier CSV.")

df['dnb_ID'] = None
df['Dates_dnb'] = None
df['page_wikipedia_de'] = None
df['infos_supp'] = None

for index, row in df.iterrows():
    nom_auteur = row['Nom_Auteur']
    dnb_id, dates_dnb, page_wikipedia_de, infos_supp = get_dnb_info(nom_auteur)
    df.at[index, 'dnb_ID'] = dnb_id
    df.at[index, 'Dates_dnb'] = dates_dnb
    df.at[index, 'page_wikipedia_de'] = page_wikipedia_de
    df.at[index, 'infos_supp'] = infos_supp


df.to_csv(output_csv, index=False, encoding='utf-8')

df_cleaned = pd.read_csv(output_csv)

df_cleaned.to_csv(output_csv, index=False, encoding='utf-8')


