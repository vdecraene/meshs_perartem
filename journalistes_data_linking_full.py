import pandas as pd
import requests
from bs4 import BeautifulSoup
import time
from urllib.parse import quote

# Fonction pour interroger l'API SolR IDref avec le nom d'un auteur
def query_idref(journalist_name):
    parts = journalist_name.split()
    if len(parts) < 2:
        print(f"Nom d'auteur invalide (doit contenir au moins un prénom et un nom) : {journalist_name}")
        return None

    first_name = parts[0]
    last_name = ' '.join(parts[1:])
    query = f"persname_t:({quote(last_name)}%20AND%20{quote(first_name)})"
    url = f"https://www.idref.fr/Sru/Solr?q={query}&wt=json&sort=score%20desc&version=2.2&start=0&rows=1&indent=on&fl=id,ppn_z,recordtype_z,affcourt_z"
    try:
        response = requests.get(url)
        response.raise_for_status()
        if response.status_code == 200:
            return response.json()
        else:
            return None
    except requests.RequestException as e:
        print(f"Erreur lors de la requête pour {journalist_name}: {e}")
        return None

# Fonction pour extraire le contenu de la balise ppn_z
def extract_ppn_z(data):
    if data and 'response' in data and 'docs' in data['response']:
        return [f"https://idref.fr/{doc.get('ppn_z', '')}.xml" for doc in data['response']['docs'] if 'ppn_z' in doc]
    return []

# Fonction pour extraire les informations des balises spécifiques à partir du XML IDref
def extract_idref_info(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'xml')

            # Initialiser les variables pour les informations recherchées
            id_viaf = None
            infos_via_api_idref = None
            id_afnor = None
            wikipedia_url = None

            # Parcourir toutes les balises subfield
            subfields = soup.find_all('subfield')
            for subfield in subfields:
                if subfield.text.startswith("http://viaf.org/"):
                    id_viaf = subfield.text
                elif "https://fr.wikipedia.org/wiki/" in subfield.text or "https://de.wikipedia.org/wiki/" in subfield.text:
                    wikipedia_url = subfield.text

            # Extraire le contenu des autres balises spécifiques
            datafield_340 = soup.find('datafield', tag="340", ind1=" ", ind2=" ")
            if datafield_340:
                subfield_a = datafield_340.find('subfield', code="a")
                if subfield_a:
                    infos_via_api_idref = subfield_a.text

            datafield_801 = soup.find('datafield', tag="801", ind1=" ", ind2="3")
            if datafield_801:
                subfield_h = datafield_801.find('subfield', code="h")
                if subfield_h:
                    id_afnor = subfield_h.text

            return id_viaf, infos_via_api_idref, id_afnor, wikipedia_url
        else:
            return None, None, None, None
    except requests.RequestException as e:
        print(f"Erreur lors de la requête pour {url}: {e}")
        return None, None, None, None

# Fonction pour interroger l'API BnF avec le nom d'un auteur
def get_bnf_info(journalist_name):
    journalist_name_encoded = quote(journalist_name)
    base_url = f"http://catalogue.bnf.fr/api/SRU?version=1.2&operation=searchRetrieve&query=aut.accesspoint%20all%20%22{journalist_name_encoded}%22"
    try:
        response = requests.get(base_url)
        response.raise_for_status()
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'xml')
            bnf_id = None
            ark_link = None
            dates_bnf = None
            infos_bio = None

            controlfield_001 = soup.find('controlfield', tag="001")
            controlfield_003 = soup.find('controlfield', tag="003")

            datafield_200 = soup.find('datafield', tag="200")
            datafield_300 = soup.find('datafield', tag="300")

            if datafield_200:
                subfield_code_f = datafield_200.find('subfield', code="f")
                if subfield_code_f:
                    dates_bnf = subfield_code_f.text

            if datafield_300:
                subfield_code_a = datafield_300.find('subfield', code="a")
                if subfield_code_a:
                    infos_bio = subfield_code_a.text

            if controlfield_001:
                bnf_id = controlfield_001.text
            if controlfield_003:
                ark_link = controlfield_003.text

            return bnf_id, ark_link, dates_bnf, infos_bio
        else:
            return None, None, None, None
    except requests.RequestException as e:
        print(f"Erreur lors de la requête pour {journalist_name}: {e}")
        return None, None, None, None

# Lire le fichier CSV avec le bon séparateur
input_csv = 'input/journalistes_perartem.csv'
output_csv = 'output/journalistes_data_linked.csv'
df = pd.read_csv(input_csv, sep='\t')

df.columns = df.columns.str.strip()

if 'Nom_Journaliste' not in df.columns:
    raise ValueError("La colonne 'journalist_name' n'existe pas dans le fichier CSV.")

df['BnF_ID'] = None
df['Lien_ARK'] = None
df['Dates_BnF'] = None
df['Infos_bio_BnF'] = None
df['ppn_z'] = None
df['ID_VIAF'] = None
df['Infos_via_API_IDref'] = None
df['ID_AFNOR'] = None
df['URL_wikipedia'] = None  # Nouvelle colonne pour URL Wikipedia

for index, row in df.iterrows():
    journalist_name = row['Nom_Journaliste']
    bnf_id, ark_link, dates_bnf, infos_bio = get_bnf_info(journalist_name)
    df.at[index, 'BnF_ID'] = bnf_id
    df.at[index, 'Lien_ARK'] = ark_link
    df.at[index, 'Dates_BnF'] = dates_bnf
    df.at[index, 'Infos_bio_BnF'] = infos_bio

    data = query_idref(journalist_name)
    ppn_z = extract_ppn_z(data)
    df.at[index, 'ppn_z'] = ', '.join(ppn_z) if ppn_z else None

    if ppn_z:
        for url in ppn_z:
            id_viaf, infos_via_api_idref, id_afnor, wikipedia_url = extract_idref_info(url)
            df.at[index, 'ID_VIAF'] = id_viaf
            df.at[index, 'Infos_via_API_IDref'] = infos_via_api_idref
            df.at[index, 'ID_AFNOR'] = id_afnor
            df.at[index, 'URL_wikipedia'] = wikipedia_url  # Enregistrer l'URL Wikipedia

    time.sleep(1)

# Enregistrer le fichier CSV initial
df.to_csv(output_csv, index=False, encoding='utf-8')

# Recharger le fichier CSV et nettoyer les guillemets intégrés
df_cleaned = pd.read_csv(output_csv)

# Supprimer les guillemets intégrés dans toutes les colonnes du DataFrame
df_cleaned.replace('"', '', regex=True, inplace=True)

# Enregistrer le fichier CSV nettoyé
df_cleaned.to_csv(output_csv, index=False, encoding='utf-8')

print(f"Traitement terminé. Les résultats ont été enregistrés dans {output_csv}")
